<?php
// TaskController.php
/**
 * Created by PhpStorm.
 * User: juriem
 * Date: 29/01/14
 * Time: 16:13
 * To change this template use File | Settings | File Templates.
 */

namespace Modera\Bundle\PublicBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class TaskController extends Controller
{
    protected $defaultValue = <<<DATA1
|0|Electronics
2|0|Video
3|0|Photo
4|1|MP3 player
5|1|TV
6|4|iPod
7|6|Shuffle
8|3|SLR
9|8|DSLR
10|9|Nikon
11|9|Canon
12|11|20
DATA1;



} 