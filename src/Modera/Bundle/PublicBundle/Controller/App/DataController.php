<?php
// DataController.php
/**
 * Created by PhpStorm.
 * User: juriem
 * Date: 24/01/14
 * Time: 16:40
 * To change this template use File | Settings | File Templates.
 */

namespace Modera\Bundle\PublicBundle\Controller\App;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;

/**
 * Class DataController
 * @package Modera\Bundle\PublicBundle\Controller\App
 *
 * @Sensio\Route("/data")
 *
 */
class DataController extends Controller
{
    /**
     *
     * @Sensio\Route("/get_users")
     *
     */
    public function getUsersAction()
    {
        $users = array();
        $users[] = array('id'=>1, 'name'=>'Ed', 'email'=>'ed@sencha.com', 'comment'=>'Some comment ....');
        $users[] = array('id'=>2, 'name'=>'Tommy', 'email'=>'tommy@sencha.com', 'comment'=>'Some comment ....');

        $responseData = array('success'=>true, 'users'=>$users);

        return new JsonResponse($responseData);
    }

    /**
     * @Sensio\Route("/update_users")
     * @Sensio\Method("post")
     *
     */
    public function updateUsersAction(Request $request)
    {
        $content = $request->getContent();

        $responseData = array('success'=>true);

        return new JsonResponse($responseData);
    }
} 