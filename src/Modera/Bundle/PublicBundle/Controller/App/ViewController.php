<?php
// ViewController.php
/**
 * Created by PhpStorm.
 * User: juriem
 * Date: 24/01/14
 * Time: 15:19
 * To change this template use File | Settings | File Templates.
 */

namespace Modera\Bundle\PublicBundle\Controller\App;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ViewController
 * @package Modera\Bundle\PublicBundle\Controller\App
 *
 * @Sensio\Route("/view", defaults={"_format"="js"})
 */
class ViewController extends Controller
{
    /**
     * @Sensio\Route("/user/List.js")
     * @Sensio\Template("ModeraPublicBundle:App/View:users_list.js.twig")
     */
    public function userListAction()
    {
        return array();
    }

    /**
     * @Sensio\Route("/user/Edit.js")
     * @Sensio\Template("ModeraPublicBundle:App/View:user_edit.js.twig")
     *
     */
    public function userEditAction()
    {

        return array();
    }
} 