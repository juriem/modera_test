<?php
// StoreController.php
/**
 * Created by PhpStorm.
 * User: juriem
 * Date: 24/01/14
 * Time: 15:57
 * To change this template use File | Settings | File Templates.
 */

namespace Modera\Bundle\PublicBundle\Controller\App;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class StoreController
 * @package Modera\Bundle\PublicBundle\Controller\App
 *
 * @Sensio\Route("/store", defaults={"_format"="js"})
 */
class StoreController extends Controller
{
    /**
     * @Sensio\Route("/Users.js")
     * @Sensio\Template("ModeraPublicBundle:App/Store:users.js.twig")
     */
    public function userAction()
    {
        return array();
    }
} 