<?php
// ControllerController.php
/**
 * Created by PhpStorm.
 * User: juriem
 * Date: 24/01/14
 * Time: 14:57
 * To change this template use File | Settings | File Templates.
 */

namespace Modera\Bundle\PublicBundle\Controller\App;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Class ControllerController
 * @package Modera\Bundle\PublicBundle\Controller\App
 *
 * @Sensio\Route("/controller", defaults={"_format"="js"})
 */
class ControllerController extends Controller
{
    /**
     * @Sensio\Route("/Users.js")
     * @Sensio\Template
     */
    public function userAction()
    {
        return array();
    }
} 