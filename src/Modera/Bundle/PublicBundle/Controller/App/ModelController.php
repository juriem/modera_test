<?php
// ModelController.php
/**
 * Created by PhpStorm.
 * User: juriem
 * Date: 24/01/14
 * Time: 16:05
 * To change this template use File | Settings | File Templates.
 */

namespace Modera\Bundle\PublicBundle\Controller\App;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ModelController
 * @package Modera\Bundle\PublicBundle\Controller\App
 *
 * @Sensio\Route("/model", defaults={"_format"="js"})
 */
class ModelController extends Controller
{
    /**
     * @Sensio\Route("/User.js")
     * @Sensio\Template("ModeraPublicBundle:App/Model:user.js.twig")
     */
    public function userAction()
    {

        return array();
    }
} 