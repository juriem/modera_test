<?php
// DefaultController.php
/**
 * Created by PhpStorm.
 * User: juriem
 * Date: 29/01/14
 * Time: 13:46
 * To change this template use File | Settings | File Templates.
 */

namespace Modera\Bundle\PublicBundle\Controller\Task2;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;

/**
 * Class DefaultController
 * @package Modera\Bundle\PublicBundle\Controller\Task2
 *
 */
class DefaultController extends Controller
{
    /**
     * @Sensio\Route
     * @Sensio\Template
     *
     */
    public function indexAction()
    {

        return array();
    }
} 