<?php

namespace Modera\Bundle\PublicBundle\Controller\Task1;

use Modera\Bundle\PublicBundle\Controller\TaskController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Modera\Bundle\ServiceBundle\Service\TreeService;

/**
 * Class DefaultController
 * @package Modera\Bundle\PublicBundle\Controller
 *
 * First task controller
 *
 */
class DefaultController extends TaskController
{
    /*
     * Actions for task #1
     */

    /**
     * @Sensio\Route
     * @Sensio\Template
     *
     * @param $name
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {

        return array('default_data'=>$this->defaultValue);
    }

    /**
     *
     * @Sensio\Route("/submit")
     * @Sensio\Method("post")
     *
     * @param Request $request
     */
    public function submitAction(Request $request)
    {
        $data = $request->get('data');

        $content = $data['content'];


        /* @var $service TreeService */
        $service = $this->get('modera_service.tree_service');

        $tree = $service->processingInputData($content);


        $responseData = array('result'=>$tree->generateTextRepresentation());

        return new JsonResponse(array('status'=>true, 'data'=>$responseData));
    }



}
