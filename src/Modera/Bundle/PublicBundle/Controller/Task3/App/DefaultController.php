<?php
// DefaultController.php
/**
 * Created by PhpStorm.
 * User: juriem
 * Date: 24/01/14
 * Time: 13:57
 * To change this template use File | Settings | File Templates.
 */

namespace Modera\Bundle\PublicBundle\Controller\Task3\App;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;

/**
 * Class DefaultController
 * @package Modera\Bundle\PublicBundle\Controller\App
 *
 * @Sensio\Route(defaults={"_format"="js"})
 *
 */
class DefaultController extends Controller
{
    /**
     * @Sensio\Route("/app.js")
     * @Sensio\Template
     */
    public function indexAction()
    {
        $debugPrefix = '';
        if ($this->get('kernel')->isDebug()){
            $debugPrefix = 'app_dev.php/';
        }

        return array('debug_prefix' => $debugPrefix);
    }

    /**
     *
     * @Sensio\Route("/app/store/Store.js")
     * @Sensio\Template("ModeraPublicBundle:Task3/App/Default:store.js.twig")
     *
     */
    public function getStoreAction()
    {
        return array();
    }
} 