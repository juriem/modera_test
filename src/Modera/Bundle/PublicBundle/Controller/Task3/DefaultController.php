<?php
// DefaultController.php
/**
 * Created by PhpStorm.
 * User: juriem
 * Date: 29/01/14
 * Time: 13:47
 * To change this template use File | Settings | File Templates.
 */

namespace Modera\Bundle\PublicBundle\Controller\Task3;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;

/**
 * Class DefaultController
 * @package Modera\Bundle\PublicBundle\Controller\Task3
 *
 */
class DefaultController extends Controller
{
    /**
     * @Sensio\Route
     * @Sensio\Template
     *
     */
    public function indexAction()
    {
        $defaultValue = <<<DATA1
|0|Electronics
2|0|Video
3|0|Photo
4|1|MP3 player
5|1|TV
6|4|iPod
7|6|Shuffle
8|3|SLR
9|8|DSLR
10|9|Nikon
11|9|Canon
12|11|20
DATA1;

        return array('default_data'=>$defaultValue);
    }

} 