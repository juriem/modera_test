<?php
// DefaultController.php
/**
 * Created by PhpStorm.
 * User: juriem
 * Date: 29/01/14
 * Time: 13:39
 * To change this template use File | Settings | File Templates.
 */

namespace Modera\Bundle\PublicBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;

/**
 * Class DefaultController
 * @package Modera\Bundle\PublicBundle\Controller
 *
 * Site entry point
 */
class DefaultController extends Controller
{
    /**
     * Redirect to task one
     *
     * @Sensio\Route
     *
     */
    public function indexAction()
    {

        return $this->redirect($this->generateUrl('modera_public_task1_default_index'));
    }
} 